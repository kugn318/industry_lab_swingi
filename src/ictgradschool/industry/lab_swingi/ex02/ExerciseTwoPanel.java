package ictgradschool.industry.lab_swingi.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple JPanel that allows users to add or subtract numbers.
 *
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseTwoPanel extends JPanel implements ActionListener {

    /**
     * Creates a new ExerciseFivePanel.
     */

    private JTextField firstNumberText, secondNumberText, resultText;
    private JButton addButton, subtractButton;

    public ExerciseTwoPanel() {
        setBackground(Color.white);

        this.firstNumberText = new JTextField(10);
        this.secondNumberText = new JTextField(10);
        this.resultText = new JTextField(10);

        this.addButton = new JButton("Add");
        this.addButton.addActionListener(this);
        this.subtractButton = new JButton("Subtract");
        this.subtractButton.addActionListener(this);

        JLabel resultLabel = new JLabel("Results: ");

        this.add(firstNumberText);
        this.add(secondNumberText);
        this.add(addButton);
        this.add(subtractButton);
        this.add(resultLabel);
        this.add(resultText);

    }

    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == addButton){
            double first = Double.parseDouble(firstNumberText.getText());
            double second = Double.parseDouble(secondNumberText.getText());
            double result = first + second;

            resultText.setText(String.valueOf(roundTo2DecimalPlaces(result)));
        }

        else if (e.getSource() == subtractButton){
            double first = Double.parseDouble(firstNumberText.getText());
            double second = Double.parseDouble(secondNumberText.getText());
            double result = first - second;

            resultText.setText(String.valueOf(roundTo2DecimalPlaces(result)));
        }

    }

    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}