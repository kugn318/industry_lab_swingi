package ictgradschool.industry.lab_swingi.ex04;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Displays an animated balloon.
 */
public class ExerciseFourPanel extends JPanel implements ActionListener,KeyListener {

    private  Balloon balloon1, balloon2, balloon3, balloon4, balloon5;
    private  JButton moveButton;
    private Timer timer;

    /**
     * Creates a new ExerciseFourPanel.
     */
    public ExerciseFourPanel() {
        setBackground(Color.white);

        this.balloon1 = new Balloon(30, 60);
        this.balloon2 = new Balloon(40, 70);
        this.balloon3 = new Balloon(50, 60);
        this.balloon4 = new Balloon(60, 70);
        this.balloon5 = new Balloon(70, 80);

        this.moveButton = new JButton("Move balloon");
        this.moveButton.addActionListener(this);
        this.add(moveButton);
        this.addKeyListener(this);
        this.timer = new Timer(200, this);

    }

    /**
     * Moves the balloon and calls repaint() to tell Swing we need to redraw ourselves.
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        balloon1.move();
        balloon2.move();
        balloon3.move();
        balloon4.move();
        balloon5.move();

        // Sets focus to the panel itself, rather than the JButton. This way, the panel can continue to generate key
        // events even after we've clicked the button.
        requestFocusInWindow();

        repaint();
    }

    /**
     * Draws any balloons we have inside this panel.
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);


        balloon1.draw(g);
        balloon2.draw(g);
        balloon3.draw(g);
        balloon4.draw(g);
        balloon5.draw(g);



        
        // Sets focus outside of actionPerformed so key presses work without pressing the button
        requestFocusInWindow();
    }

    @Override
    public void keyPressed(KeyEvent e){
        timer.start();

        if(e.getKeyCode() == KeyEvent.VK_UP){
            balloon1.setDirection(Direction.Up);
            balloon2.setDirection(Direction.Up);
            balloon3.setDirection(Direction.Up);
            balloon4.setDirection(Direction.Up);
            balloon5.setDirection(Direction.Up);
        }

        if(e.getKeyCode() == KeyEvent.VK_DOWN){
            balloon1.setDirection(Direction.Down);
            balloon2.setDirection(Direction.Down);
            balloon3.setDirection(Direction.Down);
            balloon4.setDirection(Direction.Down);
            balloon5.setDirection(Direction.Down);
        }

        if(e.getKeyCode() == KeyEvent.VK_LEFT){
            balloon1.setDirection(Direction.Left);
            balloon2.setDirection(Direction.Left);
            balloon3.setDirection(Direction.Left);
            balloon4.setDirection(Direction.Left);
            balloon5.setDirection(Direction.Left);
        }

        if(e.getKeyCode() == KeyEvent.VK_RIGHT){
            balloon1.setDirection(Direction.Right);
            balloon2.setDirection(Direction.Right);
            balloon3.setDirection(Direction.Right);
            balloon4.setDirection(Direction.Right);
            balloon5.setDirection(Direction.Right);
        }

        if(e.getKeyCode() == KeyEvent.VK_S){
           timer.stop();
        }
    }

    @Override
    public void keyReleased(KeyEvent e){

    }

    @Override
    public void	keyTyped(KeyEvent e){

    }

}