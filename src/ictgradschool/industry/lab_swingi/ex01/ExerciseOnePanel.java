package ictgradschool.industry.lab_swingi.ex01;

import com.sun.org.apache.xpath.internal.SourceTree;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple GUI app that does BMI calculations.
 */
public class ExerciseOnePanel extends JPanel implements ActionListener {

    // TODO Declare JTextFields and JButtons as instance variables here.
    private JButton calculateBMIButton, calculateWeightButton;
    private JTextField userheight, userWeight, calculatedBMI, calculatedWeight;

    /**
     * Creates a new ExerciseOnePanel.
     */
    public ExerciseOnePanel() {
        setBackground(Color.white);

        // TODO Construct JTextFields and JButtons.
        // HINT: Declare them as instance variables so that other methods in this class (e.g. actionPerformed) can
        // also access them.
        this.calculateBMIButton = new JButton("Calculate BMI");
        this.calculateWeightButton = new JButton("Calculate Healthy Weight");
        this.userheight = new JTextField(10);
        this.userWeight = new JTextField(10);
        this.calculatedBMI = new JTextField(10);
        this.calculatedWeight = new JTextField(10);

        // TODO Declare and construct JLabels
        // Note: These ones don't need to be accessed anywhere else so it makes sense just to declare them here as
        // local variables, rather than instance variables.

        JLabel heightLabel = new JLabel("Height in metres: ");
        JLabel weightLabel = new JLabel("Weight in kilograms: ");
        JLabel bmiLabel = new JLabel("Your body mass index (BMI) is: ");
        JLabel healthyWeightLabel = new JLabel("Maximum healthy weight for your height: ");


        // TODO Add JLabels, JTextFields and JButtons to window.
        // Note: The default layout manager, FlowLayout, will be fine (but feel free to experiment with others if you want!!)
        this.add(heightLabel);
        this.add(userheight);
        this.add(weightLabel);
        this.add(userWeight);
        this.add(calculateBMIButton);
        this.add(bmiLabel);
        this.add(calculatedBMI);
        this.add(calculateWeightButton);
        this.add(healthyWeightLabel);
        this.add(calculatedWeight);

        // TODO Add Action Listeners for the JButtons
        this.calculateBMIButton.addActionListener(this);
        this.calculateWeightButton.addActionListener(this);
    }


    /**
     * When a button is clicked, this method should detect which button was clicked, and display either the BMI or the
     * maximum healthy weight, depending on which JButton was pressed.
     */
    public void actionPerformed(ActionEvent event) {

        // TODO Implement this method.
        // Hint #1: event.getSource() will return the button which was pressed.
        // Hint #2: JTextField's getText() method will get the value in the text box, as a String.
        // Hint #3: JTextField's setText() method will allow you to pass it a String, which will be diaplayed in the text box.

        if (event.getSource() == calculateBMIButton){
            double height = Double.parseDouble(userheight.getText());
            double weight = Double.parseDouble(userWeight.getText());
            double bmi = weight/(height*height);

            calculatedBMI.setText(String.valueOf(roundTo2DecimalPlaces(bmi)));
        }
        else if(event.getSource() == calculateWeightButton){
            double height = Double.parseDouble(userheight.getText());
            double healthyWeight = 24.9 * height * height;

            calculatedWeight.setText(String.valueOf(roundTo2DecimalPlaces(healthyWeight)));
        }

    }


    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}